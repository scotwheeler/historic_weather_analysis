# Historic Weather Analysis for Flexibility Forecasting

Takes 20 years historic reanalysis weather data (downloaded through the Renewable.Ninja interface), 
and calculates normalised average daily hourly temperature profiles for a particular day of the year (MM-DD) and the standard deviation. 
This profile can be either multiplied by a forecasted daily temperature, or the historic average daily temperature, to obtain a profile forecast.
